# Venom auth

This project is a template of a venom (Vue,
Express, Node and Mongo) stack that can be cloned
and used as a base for other projects.

Includes user registration and login authentication.
Sample pages are provided for demonstration and
reference.

[Docker](https://www.docker.com/) is required to run this project.

### Included
- [Vue](https://vuejs.org/)
- [Express](https://expressjs.com/)
- [Node](https://nodejs.org/en/)
- [Mongo](https://www.mongodb.com/)
- [Mongoose](https://mongoosejs.com/) and templates
- Pre-configured Express routes
- User authentication
- [Socket.io](https://socket.io/docs/)
- [Sass](https://sass-lang.com/) support
- [Vue router](https://router.vuejs.org/)
- [Vuex](https://vuex.vuejs.org/)
- [Axios](https://github.com/axios/axios)
- [Webpack](https://webpack.js.org/)
- [Bootstrap](https://getbootstrap.com/)
- [FontAwesome](https://fontawesome.com/)

### How to use
Start by cloning the project repository

`git clone https://gitlab.com/stephencampbell/venom-auth.git`

Next, navigate to the project directory

`cd venom-auth`

Start docker-compose to run the server and Mongo docker containers

`docker-compose up`

Install dependencies

`npm install`

Bundle frontend assets with Webpack

`npm run client:watch`

### Notes
- When making changes in `docker-compose.yml`
  or `Dockerfile`, run `docker-compose up --build`
  to see changes