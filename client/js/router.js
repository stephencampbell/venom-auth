import Vue       from 'vue'
import VueRouter from 'vue-router'

import Home     from './components/Home'
import Login    from './components/Login'
import Register from './components/Register'
import Profile  from './components/Profile'
import Posts    from './components/Posts'
import Chat     from './components/Chat'

Vue.use(VueRouter)

const routes = [
  { path: '/', component: Home },
  { path: '/login', component: Login },
  { path: '/register', component: Register },
  { path: '/profile/:slug', component: Profile },
  { path: '/posts/', component: Posts },
  { path: '/chat/', component: Chat }
]

export default new VueRouter({
  mode: 'history',
  routes
})