// Node modules
import Vue from 'vue'

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

// Vue components
import Frame from './components/Frame'

// Styling
import 'bootstrap/dist/css/bootstrap'
import 'bootstrap-vue/dist/bootstrap-vue'
import '../sass/main'

// Vuex
import store from './store'

// Vue router
import router from './router'

// Vue
const app = new Vue({
  el:     '#app',
  router,
  store,
  render: h => h(Frame)
})