import Vue                  from 'vue'
import Vuex                 from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import axios                from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state:     {
    authToken: null,
    user:      null,
    users:     [],
    posts:     []
  },
  getters:   {
    authToken: state => state.authToken,
    user:      state => state.user,
    users:     state => state.users,
    posts:     state => state.posts
  },
  mutations: {
    setAuthToken (state, token) {
      state.authToken = token
    },
    setUser (state, user) {
      state.user = user
    },
    setUsers (state, users) {
      state.users = users
    },
    setPosts (state, posts) {
      state.posts = posts
    },
    addPost (state, post) {
      state.posts.push(post)
    },
    clearAuthData (state) {
      state.authToken = null
      state.user = null
    }
  },
  actions:   {
    loadUsers ({ getters, commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get('/api/users/all', {
            headers: {
              Authorization: getters.authToken
            }
          })
          .then(res => {
            commit('setUsers', res.data)
            resolve(res.data)
          })
          .catch(err => {
            if(err.response) {
              if(err.response.status === 401) {
                commit('clearAuthData')
              }
            }
            reject(err)
          })
      })
    },
    newPost ({ getters, commit }, { title, content }) {
      return new Promise((resolve, reject) => {
        axios
          .post('/api/posts/add', {
            title,
            content
          }, {
            headers: {
              Authorization: getters.authToken
            }
          })
          .then(res => {
            commit('addPost', res.data)
            resolve(res.data)
          })
          .catch(err => {
            if(err.response) {
              if(err.response.status === 401) {
                commit('clearAuthData')
              }
            }
            reject(err)
          })
      })
    },
    loadPosts ({ commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get('/api/posts/recent')
          .then(res => {
            commit('setPosts', res.data)
            resolve(res.data)
          })
          .catch(err => {
            if(err.response) {
              if(err.response.status === 401) {
                commit('clearAuthData')
              }
            }
            reject(err)
          })
      })
    }
  },
  plugins:   [
    createPersistedState()
  ]
})