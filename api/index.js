// Node modules
const path       = require('path')
const express    = require('express')
const mongoose   = require('mongoose')
const bodyParser = require('body-parser')
const passport   = require('passport')
const session    = require('express-session')
const morgan     = require('morgan')
const cors       = require('cors')

// Keys
const keys = require('../config/keys')

// Web socket server
const socket = require('./socket')

// Controller files
const posts = require('./controllers/posts')
const users = require('./controllers/users')

// Constants
const port           = 3000
const mongoContainer = 'mongo'
const mongoDatabase  = 'venom'
const mongoPort      = 27017
const host           = 'localhost'

// App
const app = express()

// Database
const db = `mongodb://${mongoContainer}:${mongoPort}/${mongoDatabase}` // Domain is docker container name
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log('MongoDB connected'))
  .catch(err => console.log(err))

app.use(session({
  secret:            keys.sessionKey,
  resave:            false,
  saveUninitialized: true,
  cookie:            { secure: false }
}))

// Morgan request logging
app.use(morgan('tiny'))

// CORS
app.use(cors())

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Passport middleware
app.use(passport.initialize())

// Passport configuration
require('../config/passport')(passport)

// Use controllers
app.use('/api/posts', posts)
app.use('/api/users', users)

// Static files
app.get('/', (req, res) => res.sendFile(path.join(__dirname, '/../static/index.html')))
app.use(express.static(path.join(__dirname, '/../dist')))
app.use(express.static(path.join(__dirname, '/../static')))

// Fallback route
app.all('*', (req, res) => res.sendFile(path.join(__dirname, '/../static/index.html')))

// Listen for requests
app.listen(port, () => console.log(`Listening on http://${host}:${port}`))

// Start web socket server
socket(host)