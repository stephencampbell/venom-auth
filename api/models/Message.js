const mongoose = require('mongoose')
const Schema   = mongoose.Schema

const messageSchema = new Schema({
  message:   {
    type:     String,
    required: true
  },
  createdBy: {
    type:     String,
    required: true
  },
  createdAt: {
    type:    Date,
    default: Date.now
  }
})

module.exports = Message = mongoose.model('messages', messageSchema)