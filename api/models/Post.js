const mongoose = require('mongoose')
const Schema   = mongoose.Schema

const postSchema = new Schema({
  title:     {
    type:     String,
    required: true
  },
  content:   {
    type:     String,
    required: true
  },
  slug:      {
    type:     String,
    required: true
  },
  createdBy: {
    type:     String,
    required: true
  },
  createdAt: {
    type:    Date,
    default: Date.now
  }
})

module.exports = Post = mongoose.model('posts', postSchema)