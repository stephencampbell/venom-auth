const express  = require('express')
const router   = express.Router()
const Post     = require('../models/Post')
const slugify  = require('slugify')
const passport = require('passport')

// @route GET api/posts/test
// @desc  Tests posts route
// @access Public
router.get('/test', (req, res) => res.json({ msg: 'Posts route works' }))

// @route GET api/posts/all
// @desc  Gets all posts
// @access Public
router.get('/all', (req, res) => {
  Post
    .find({})
    .then(posts => res.json(posts))
})

// @route GET api/posts/recent
// @desc  Gets recent posts
// @access Public
router.get('/recent', (req, res) => {
  Post
    .find({})
    .sort({ 'createdAt': -1 })
    .limit(10)
    .then(posts => res.json(posts))
})

// @route GET api/posts/:slug
// @desc  Gets a single post
// @access Public
router.get('/:slug', (req, res) => {
  Post
    .findOne({ slug: req.params.slug })
    .then(post => res.json(post))
})

// @route GET api/posts/by-user/:id
// @desc  Gets a user's posts
// @access Private
router.get('/by-user/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
  Post
    .find({ createdBy: req.params.id })
    .then(posts => res.json(posts))
})

// @route POST api/posts/add
// @desc  Adds a new post
// @access Private
router.post('/add', passport.authenticate('jwt', { session: false }), (req, res) => {
  if (req.user) {
    const slug = slugify(req.body.title.toLowerCase())
    Post
      .findOne({ slug })
      .then(post => {
        if (post) {
          return res.status(400).json({ msg: 'Post slug already exists' })
        } else {
          const newPost = new Post({
            createdBy: req.user._id,
            title:     req.body.title,
            content:   req.body.content,
            slug
          })

          newPost
            .save()
            .then(post => res.json(post))
            .catch(err => console.log(err))
        }
      })
  }
})

module.exports = router