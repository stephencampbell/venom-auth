const express  = require('express')
const router   = express.Router()
const User     = require('../models/User')
const slugify  = require('slugify')
const bcrypt   = require('bcryptjs')
const jwt      = require('jsonwebtoken')
const jwtKey   = require('../../config/keys').jwtKey
const passport = require('passport')

// @route  GET api/users/test
// @desc   Tests users route
// @access Public
router.get('/test', (req, res) => res.json({ msg: 'Users route works' }))

// @route  GET api/users/current
// @desc   Returns current user
// @access Private
router.get('/current', passport.authenticate('jwt', { session: false }), (req, res) => {
  return res.json(req.user)
})

// @route  GET api/users/all
// @desc   Gets all users
// @access Private
router.get('/all', passport.authenticate('jwt', { session: false }), (req, res) => {
  User
    .find({})
    .then(users => res.json(users.map(u => ({
      id:        u.id,
      firstName: u.firstName,
      lastName:  u.lastName,
      slug:      u.slug
    }))))
})

// @route  GET api/users/:slug
// @desc   Gets a single user
// @access Private
router.get('/:slug', passport.authenticate('jwt', { session: false }), (req, res) => {
  User
    .findOne({ slug: req.params.slug })
    .then(u => {
      const user = {
        id:        u.id,
        firstName: u.firstName,
        lastName:  u.lastName,
        slug:      u.slug
      }
      res.json(user)
    })
})

// @route  POST api/users/register
// @desc   Adds a new user
// @access Public
router.post('/register', (req, res) => {
  const firstName    = req.body.firstName.trim()
  const lastName     = req.body.lastName.trim()
  const slug         = slugify(`${firstName.toLowerCase()} ${lastName.toLowerCase()}`)
  const emailAddress = req.body.emailAddress.trim()

  User
    .findOne({ emailAddress })
    .then(user => {
      if (user) {
        return res.status(400).json({ msg: 'Email address already registered' })
      } else {
        // Create mongoose user object
        const newUser = new User({
          firstName,
          lastName,
          emailAddress,
          password: req.body.password,
          slug
        })

        // Generate 10 character salt for password
        bcrypt.genSalt(10, (err, salt) => {
          if (err) throw err

          // Hash password
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err

            newUser.password = hash

            // Save user
            newUser
              .save()
              .then(user => res.json(user))
              .catch(err => console.log(err))
          })
        })
      }
    })
})

// @route  POST api/users/login
// @desc   Login user / return JWT token
// @access Public
router.post('/login', (req, res) => {
  const emailAddress = req.body.emailAddress.trim()
  const password     = req.body.password

  // Find user by email address
  User
    .findOne({ emailAddress })
    .then(user => {
      if (user) {
        // Compare plain text password to hashed password
        bcrypt.compare(password, user.password)
              .then(isMatch => {
                if (isMatch) {
                  // Store login in the session
                  const ssn = req.session
                  ssn.user  = user

                  // Create JWT payload
                  const payload = {
                    id:        user.id,
                    firstName: user.firstName,
                    lastName:  user.lastName
                  }

                  // Sign token
                  jwt.sign(payload, jwtKey, { expiresIn: 3600 }, (err, token) => {
                    // Send token as response
                    res.json({
                      token: `Bearer ${token}`,
                      user:  {
                        id:           user.id,
                        firstName:    user.firstName,
                        lastName:     user.lastName,
                        emailAddress: user.emailAddress,
                        slug:         user.slug
                      }
                    })
                  })
                } else {
                  return res.status(400).json({ msg: 'Incorrect password' })
                }
              })
      } else {
        return res.status(404).json({ msg: 'Email address not registered' })
      }
    })
})

module.exports = router