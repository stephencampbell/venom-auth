const express = require('express')

// Messsage model
const Message = require('./models/Message')

const webSocketPort = 80

// Set up server
const webSocket = express()
const server    = require('http').Server(webSocket)
const io        = require('socket.io')(server)

// List of active user names
let users = []

// Web sockets
io.on('connection', (socket, data) => {
  socket.userName = socket.handshake.query.userName

  users.push({
    userName: socket.userName
  })

  io.sockets.emit('users-updated', { users })

  // On connection, load recent messages
  Message
    .find({})
    .sort({ 'createdAt': -1 })
    .limit(10)
    .then(messages => {
      socket.emit('messages-loaded', messages)
    })

  // Receive sent messages from the client
  socket.on('send-message', function (data) {
    const userId  = data.userId
    const message = data.message

    const newMessage = new Message({
      createdBy: userId,
      message:   message
    })

    newMessage
      .save()
      .then(message => {
        // Broadcast new message
        io.sockets.emit('message-saved', message)
      })
      .catch(err => console.log(err))
  })

  socket.on('disconnect', function (data) {
    users = users.filter(u => u.userName !== socket.userName)
    io.sockets.emit('users-updated', { users })
  })
})

module.exports = host => {
  // Web socket server
  server.listen(webSocketPort, () => console.log(`Web socket server listening on http://${host}:${webSocketPort}`))
}
